"""django_test URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url

from . import views, testdb

urlpatterns = [
    url(r'^hello$', views.hello),
    url(r'^add_db$', testdb.add_db),
    url(r'^get_db$', testdb.get_db),
    url(r'^updata_db', testdb.updata_db),
    url(r'^del_db', testdb.del_db),
]
