# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.shortcuts import render

from TestModel.models import Test


def add_db(request):
    test1 = Test(name="张三")
    test1.save()

    test = Test.objects.create(name='haha')
    return HttpResponse('<p>添加{}数据成功！</p>'.format(test.name))


def get_db(request):
    test_list = Test.objects.all()
    data = []
    for test in test_list:
        data_dict = {}
        data_dict["name"] = test.name
        data.append(data_dict)

    test1 = Test.objects.get(id=1)

    test2 = Test.objects.filter(name='张三').first()

    # 限制返回的数据 相当于 SQL 中的 OFFSET 0 LIMIT 2;
    # Test.objects.order_by('name')[0:2]

    # 数据排序
    # Test.objects.order_by("id")

    context = {"data": data, 'test1': test1.name, 'test2': test2.name}

    return render(request, 'hello.html', context)


def updata_db(request):
    test = Test.objects.get(id=1)
    test.name = '李四'
    test.save()

    Test.objects.filter(name='张三').update(name='王五')

    return HttpResponse("修改数据成功！")


def del_db(request):
    # test = Test.objects.get(name='李四')
    # test.delete()

    Test.objects.filter(name='王五').delete()

    return HttpResponse("删除成功！")

