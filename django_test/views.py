from django.http import HttpResponse
from django.shortcuts import render


def hello(request):
    # return HttpResponse("Hello World!")
    context = {}
    context["hello"] = "Hello World!"
    return render(request, 'hello.html', context)


